<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_kasir extends CI_Model {

	public function getDataKasir()
	{
		return $this->db->get('user')->result();
	}
	public function simpan_kasir()
	{
		$object = array(
			'namauser' => $this->input->post('namauser') ,
			'username' => $this->input->post('username') ,
			'password' => $this->input->post('password') ,
			'level' => "kasir",
			'id_user' => $this->input->post('id_user') 
		);
		return $this->db->insert('user', $object);
	}
	public function hapus($id_user)
	{
		return $this->db->where('id_user', $id_user)->delete('user');
	}
	public function edit_kasir()
	{
		$object = array(
			'username' => $this->input->post('username') ,
			'password' => $this->input->post('password') ,
			'level' => "kasir",
			'id_user' => $this->input->post('id_user') 
		);
		return $this->db->where('id_user', $this->input->post('id_user'))->update('user', $object);
	}
	public function detail($a)
	{
		return $this->db->where('id_user', $a)->get('user')->row();
	}
}

/* End of file M_user.php */
/* Location: ./application/models/M_user.php */