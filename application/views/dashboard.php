						<div class="panel-body">
							<div class="row">
							<?php if ($this->session->userdata('level')=="admin"): ?>	
								<div class="col-md-3">
									<div class="metric">
									<span class="icon"><i class="fa fa-book"></i></span>
										<p>
											<span class="number"><a href="<?=base_url('index.php/buku')?>">- BUKU -</a></span>
											<span class="title">buku</span>
										</p>
									</div>
								</div>
								<div class="col-md-3">
									<div class="metric">
									<span class="icon"><i class="fa fa-list"></i></span>
										<p>
											<span class="number"><a href="<?=base_url('index.php/kategori')?>">- KATEGORI -</a></span>
											<span class="title">kategori</span>
										</p>
									</div>
								</div>
								<div class="col-md-3">
									<div class="metric">
									<span class="icon"><i class="fa fa-user"></i></span>
										<p>
											<span class="number"><a href="<?=base_url('index.php/kasir')?>">- KASIR-</a></span>
											<span class="title">kasir</span>
										</p>
									</div>
								</div>
								<?php else: ?>
								<div class="col-md-3">
									<div class="metric">
									<span class="icon"><i class="fa fa-shopping-cart"></i></span>
										<p>
											<span class="number"><a href="<?=base_url('index.php/transaksi')?>">- TRANSAKSI - </a></span>
											<span class="title">transaksi</span>
									</div>
								</div>
								<?php endif ?>
							</div>
						</div>
						