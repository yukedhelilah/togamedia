<div class="panel">
	<div class="panel-heading">
		<h3 class="panel-title">Data User</h3>
	</div>
	<div class="panel-body">
		<?php if ($this->session->flashdata('pesan')!=null): ?>
			<div class="alert alert-info alert-dismissible" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<?=$this->session->flashdata('pesan')?>
			</div>
		<?php endif ?>
		
		<table class="table table-bordered table-striped table-hover js-basic-example dataTable">
			<thead>
				<th>Id User</th>
				<th>Nama User</th>
				<th>Username</th>
				<th>Password</th>
				<th>Level</th>
				<th>Aksi</th>
			</thead>
			<tbody>
				<?php foreach ($tampil_kasir as $user): ?>
					<tr>
					<?php if ($user->level == "kasir"): ?>
						<td><?=$user->id_user?> </td>
						<td><?=$user->namauser?> </td>
						<td><?=$user->username?></td>
						<td><?=$user->password?> </td>
						<th><?=$user->level?></th>
						<td>
							<a href="#edit" onclick="edit(<?=$user->id_user?>)" class="btn btn-warning" data-toggle="modal">Ubah</a>
							<a href="<?=base_url('index.php/kasir/hapus/'.$user->id_user)?>" class="btn btn-danger" onclick="return confirm('Apakah anda yakin?')">Hapus</a>
						</td>
					<?php else: ?>
						<td><?=$user->id_user?> </td>
						<td><?=$user->namauser?> </td>
						<td><?=$user->username?></td>
						<td><?=$user->password?> </td>
						<th><?=$user->level?></th>
						<td></td>
					<?php endif ?>
					</tr>
				<?php endforeach ?>
			</tbody>
		</table>
		<a href="#modalTambah" data-toggle="modal" class="btn btn-success" style="padding-left: 20px;padding-right: 20px;padding-top:10px;padding-bottom: 10px;font-size: 17px;">Tambah</a>
		<div class="modal fade" id="modalTambah">
		<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="<?=base_url('index.php/kasir/tambah_kasir')?>" method="POST">
					<table class="table">
						<tr>
							<td>Nama user</td>
							<td><input type="text" name="namauser" class="form-control"><br>
						</tr>
						<tr>
							<td>Username</td>
							<td><input type="text" name="username" class="form-control"><br>
						</tr>
						<tr>
							<td>Password</td>
							<td><input type="password" name="password" class="form-control"><br>

							<input type="submit" name="simpan" value="Simpan" class="btn btn-success">
							</td>
						</tr>
					</table>
				</form>
			</div>
		</div>	
		</div>	
		</div>
		<div class="modal fade" id="edit">
		<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="<?=base_url('index.php/kasir/ubah')?>" method="POST">
					<table class="table">
						<tr>
							<td>Nama user</td>
							<td><input id="namauser" type="text" name="namauser" class="form-control"><br>
						</tr>
						<tr>
							<td>Username</td>
							<td><input id="username" type="text" name="username" class="form-control"><br>
						</tr>
						<tr>
							<td>Password</td>
							<td><input id="password" type="password" name="password" class="form-control"><br>

						<input type="hidden" name="id_user" id="id_user">
						<input type="submit" name="simpan" value="Simpan" class="btn btn-success">
							</td>
						</tr>
					</table>
				</form>
			</div>
		</div>	
		</div>	
		</div>
	</div>
</div>
<script type="text/javascript">
	function edit(a) {
		$.ajax({
			type:"post",
			url:"<?=base_url()?>index.php/kasir/edit_kasir/"+a,
			dataType:"json",
			success:function(data){
				$("#id_user").val(data.id_user);
				$("#namauser").val(data.namauser);
				$("#username").val(data.username);
				$("#password").val(data.password);
			}
		});
	}
</script>